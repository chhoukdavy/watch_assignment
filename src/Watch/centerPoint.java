package Watch;

import java.awt.*;

public class centerPoint {
    public static Point getCenterPoint(int frameW, int frameH) {
        Point p = new Point();

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frameW) / 2);
        int y = (int) ((dimension.getHeight() - frameH) / 2);

        p.x = x;
        p.y = y;

        return p;
    }
}
