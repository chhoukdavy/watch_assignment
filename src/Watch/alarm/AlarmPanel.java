package Watch.alarm;

import Watch.MainFrame;
import Watch.MainPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class AlarmPanel extends JPanel {

    public AlarmPanel() {
        setLayout(null);
        setVisible(true);
        setBounds(0, 0, 500, 500);

        JLabel alarmTitle = new JLabel("Alarm Clock");
        alarmTitle.setFont(new Font("Courier", Font.BOLD, 20));
        alarmTitle.setBounds((500 - 115)/2, 20, 115, 30);
        add(alarmTitle);

        ImageIcon icon = new ImageIcon(MainFrame.class.getResource("/alarmclock.gif"));
        JLabel alarmIcon = new JLabel(icon);
        alarmIcon.setBounds(35, 15, icon.getIconWidth(), icon.getIconHeight());
        add(alarmIcon);

        JLabel alarmRining = new JLabel("Ting Ting Ting...");
        alarmRining.setFont(new Font("Courier", Font.PLAIN, 18));
        alarmRining.setBounds((500 - 150)/2, 355, 150, 30);
        add(alarmRining);

        Font fontBtn = new Font("Courier", Font.PLAIN, 14);
        int WidthBtn = 85;
        int HieghtBtn = 25;

        JButton btnStop = new JButton("Stop");
        btnStop.setFont(fontBtn);
        btnStop.setBounds(150, 420, WidthBtn, HieghtBtn);
        btnStop.addActionListener(this::onStopClick);
        add(btnStop);

        JButton btnSnooze = new JButton("Snooze");
        btnSnooze.setFont(fontBtn);
        btnSnooze.setBounds(250, 420, WidthBtn, HieghtBtn);
        btnSnooze.addActionListener(this::onSnoozeClick);
        add(btnSnooze);
    }

    private void onStopClick(ActionEvent e) {
        MainPanel.setEnableAlarm(false);
        MainPanel.setIsAlarming(false);
        MainPanel.setIsAlarmClockThreadLive(false);
        MainPanel.setSoundPlay(false);
        MainPanel.lblAlarmIcon.setVisible(false);
        MainPanel.alarmFrame.setVisible(false);
    }

    private void onSnoozeClick(ActionEvent e) {
        MainPanel.setEnableAlarm(true);
        MainPanel.setSoundPlay(false);
        MainPanel.lblAlarmIcon.setVisible(true);
        MainPanel.setAlarmSnooze(30);
        MainPanel.alarmFrame.setVisible(false);
    }
}
