package Watch.alarm;

import Watch.MainPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

class InputDialogBoxPanel extends JPanel {
    private int hourSelected;
    private int minuteSelected;
    private int secondSelected;

    private boolean isAmAlarm = true;

    private JComboBox cbInputHour;

    private JComboBox cbInputMinute;
    private JComboBox cbInputSecond;
    private JComboBox cbAmPM;

    InputDialogBoxPanel() {
        setLayout(null);
        setBounds(0, 0, 300, 200);
        setVisible(true);
        setBackground(new Color(8, 151, 255, 100));

        JLabel setAlarmS = new JLabel("Set Alarm Clock");
        Font fontTitle = new Font("Courier", Font.BOLD, 20);
        setAlarmS.setFont(fontTitle);
        setAlarmS.setBounds(125, 10, 150, 20);
        add(setAlarmS);

        JLabel alarmTitleLabel = new JLabel("HH   :  MM   :  SS    AM/PM");
        alarmTitleLabel.setFont(new Font("Courier", Font.PLAIN, 18));
        alarmTitleLabel.setBounds(90, 45 , 250, 40);
        add(alarmTitleLabel);

        JLabel lblCopyRight = new JLabel("@2016 all rights reserved.");
        Font allRRFont = new Font("Courier", Font.PLAIN, 10);
        lblCopyRight.setFont(allRRFont);
        lblCopyRight.setBounds(117, 170, 175, 20);
        add(lblCopyRight);

        Font cbFont = new Font("Arial", Font.BOLD, 12);
        int cbHeight = 90;

        String[] inputHour = new String[12];
        //setTheContent of AM String
        for(int i = 1; i < 13; i++) {
            if(i < 10) {
                inputHour[i - 1]  = "0" + i;
            } else {
                inputHour[i - 1]  = "" + i;
            }
        }

        cbInputHour = new JComboBox(inputHour);
        cbInputHour.setSelectedIndex(getSelectedIndex(inputHour, MainPanel.getHour()));
        hourSelected = getSelectedIndex(inputHour, MainPanel.getHour()) + 1;
        cbInputHour.setFont(cbFont);
        cbInputHour.addActionListener(this::onHourComboSelection);
        cbInputHour.setBounds(90, cbHeight, 50, 30);
        add(cbInputHour);

        String[] inputMinute = new String[60];
        for(int i = 0; i < 60; i++) {
            if(i < 10) {
                inputMinute[i] = "0" + i;
            } else {
                inputMinute[i] = "" + i;
            }
        }
        cbInputMinute = new JComboBox(inputMinute);
        cbInputMinute.setSelectedIndex(getSelectedIndex(inputMinute, MainPanel.getMinute()));
        minuteSelected = getSelectedIndex(inputMinute, MainPanel.getMinute());
        cbInputMinute.setFont(cbFont);
        cbInputMinute.addActionListener(this::onMinuteComboSelection);
        cbInputMinute.setBounds(145, cbHeight, 50, 30);
        add(cbInputMinute);

        String[] inputSecond = new String[60];
        for(int i = 0; i < 60; i++) {
            if(i < 10)
                inputSecond[i] = "0" + i;
            else
                inputSecond[i] = "" + i;
        }
        cbInputSecond = new JComboBox(inputSecond);
        cbInputSecond.setSelectedIndex(getSelectedIndex(inputSecond, MainPanel.getSecond()));
        secondSelected = getSelectedIndex(inputSecond, MainPanel.getSecond());
        cbInputSecond.setFont(cbFont);
        cbInputSecond.addActionListener(this::onSecondComboSelection);
        cbInputSecond.setBounds(200, cbHeight, 50, 30);
        add(cbInputSecond);

        String[] amPm = new String[2];
        amPm[0] = "AM";
        amPm[1] = "PM";
        cbAmPM = new JComboBox(amPm);
        cbAmPM.setFont(cbFont);
        cbAmPM.setSelectedIndex(MainPanel.isAM()?0:1);
        isAmAlarm = MainPanel.isAM() ? true : false;
        cbAmPM.addActionListener(this::comboSelectedAction);
        cbAmPM.setBounds(255, cbHeight, 50, 30);
        add(cbAmPM);

        Font fontBtn = new Font("Courier", Font.PLAIN, 14);
        int btnWidth = 85;
        int btnHieght = 25;

        JButton btnOK = new JButton("OK");
        btnOK.setFont(fontBtn);
        btnOK.setBounds(105, 135, btnWidth, btnHieght);
        btnOK.addActionListener(this::onOkayClicked);
        add(btnOK);

        JButton btnCancel = new JButton("Cancel");
        btnCancel.setFont(fontBtn);
        btnCancel.setBounds(210, 135, btnWidth, btnHieght);
        btnCancel.addActionListener(this::onCancelClicked);
        add(btnCancel);
    }

    private void onOkayClicked(ActionEvent e) {
        MainPanel.setIsAlarming(true);
        MainPanel.setIsAlarmClockThreadLive(true);
        MainPanel.setAlarmHour(hourSelected);
        MainPanel.setAlarmMinute(minuteSelected);
        MainPanel.setAlarmSecond(secondSelected);
        MainPanel.setIsAlarmAm(isAmAlarm);
        MainPanel.setEnableAlarm(true);
        MainPanel.lblAlarmIcon.setVisible(true);
        MainPanel.inputFrameSetAlarm.setVisible(false);
    }

    private void onCancelClicked(ActionEvent e) {
        MainPanel.setIsAlarmClockThreadLive(false);
        MainPanel.setIsAlarming(false);
        MainPanel.inputFrameSetAlarm.setVisible(false);
    }

    private void onHourComboSelection(ActionEvent e) {
        if(e.getSource() == cbInputHour) {
            hourSelected = cbInputHour.getSelectedIndex() + 1;
        }
    }

    private void onMinuteComboSelection(ActionEvent e) {
        if(e.getSource() == cbInputMinute) {
            minuteSelected = cbInputMinute.getSelectedIndex();
        }
    }

    private void onSecondComboSelection(ActionEvent e) {
        if(e.getSource() == cbInputSecond) {
            secondSelected = cbInputSecond.getSelectedIndex();
        }
    }

    private void comboSelectedAction(ActionEvent e) {
        if(e.getSource() == cbAmPM) {
            String selectedString = (String) cbAmPM.getSelectedItem();
            if(selectedString.equals("AM")) {
                isAmAlarm = true;
            } else {
                isAmAlarm = false;
            }
        }
    }

    private int getSelectedIndex(String[] arr, int x) {
        int y = 0;
        for(int i = 1; i < arr.length; i++) {
            if(x < 10) {
                if(("0" + x).equalsIgnoreCase(arr[i]))
                    y = i;
            } else {
                if(("" + x).equalsIgnoreCase(arr[i]))
                    y = i;
            }
        }
        return y;
    }
}
