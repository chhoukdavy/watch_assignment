package Watch.alarm;

import Watch.centerPoint;

import javax.swing.*;
import java.awt.*;

public class InputDialogBoxFrame extends JFrame {
    public InputDialogBoxFrame(boolean vis) {
        setUndecorated(true);
        setVisible(vis);
        Point centerP = centerPoint.getCenterPoint(300, 200);
        setBounds(centerP.x - 50, centerP.y, 400, 200);
        setContentPane(new InputDialogBoxPanel());
    }
}
