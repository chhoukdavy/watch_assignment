package Watch.alarm;

import Watch.centerPoint;

import javax.swing.*;
import java.awt.*;

public class AlarmFrame extends JFrame{
    public AlarmFrame(boolean vis) {
        setUndecorated(true);
        setVisible(vis);
        Point cpoint = centerPoint.getCenterPoint(500, 500);
        setBounds(cpoint.x, cpoint.y, 500, 500);
        setContentPane(new AlarmPanel());
    }
//    public static void main(String[] args) {
//        AlarmFrame frame = new AlarmFrame(true);
//    }
}
