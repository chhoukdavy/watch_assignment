package Watch.countDown;

import Watch.centerPoint;

import javax.swing.*;
import java.awt.*;

public class CountDownFrame extends JFrame {
    public CountDownFrame(boolean vis) {
        setUndecorated(true);
        setVisible(vis);
        Point cpoint = centerPoint.getCenterPoint(500, 500);
        setBounds(cpoint.x, cpoint.y, 500, 500);
        setContentPane(new CountDownPanel());
    }
}
