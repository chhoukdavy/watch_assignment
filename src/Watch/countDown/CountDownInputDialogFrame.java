package Watch.countDown;

import Watch.centerPoint;

import javax.swing.*;
import java.awt.*;

public class CountDownInputDialogFrame extends JFrame{
    public CountDownInputDialogFrame(boolean vis) {
        setUndecorated(true);
        setVisible(vis);
        Point centerP = centerPoint.getCenterPoint(300, 200);
        setBounds(centerP.x - 50, centerP.y, 400, 200);
        setContentPane(new CountDownInputDialogPanel());
    }
}
