package Watch.countDown;

import Watch.MainPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class CountDownInputDialogPanel extends JPanel{
    private int hourSelected;
    private int minuteSelected;
    private int secondSelected;

    private JComboBox cbInputHour;
    private JComboBox cbInputMinute;
    private JComboBox cbInputSecond;

    CountDownInputDialogPanel() {
        setLayout(null);
        setBounds(0, 0, 300, 200);
        setVisible(true);
        setBackground(new Color(8, 151, 255, 100));

        JLabel setCountDownTime = new JLabel("Set Count Down");
        Font fontTitle = new Font("Courier", Font.BOLD, 20);
        setCountDownTime.setFont(fontTitle);
        setCountDownTime.setBounds(125, 10, 175, 20);
        add(setCountDownTime);

        JLabel cdTitleLabel = new JLabel("HH   :  MM   :  SS");
        cdTitleLabel.setFont(new Font("Courier", Font.PLAIN, 18));
        cdTitleLabel.setBounds(130, 45 , 170, 40);
        add(cdTitleLabel);

        JLabel lblCopyRight = new JLabel("@2016 all rights reserved.");
        Font allRRFont = new Font("Courier", Font.PLAIN, 10);
        lblCopyRight.setFont(allRRFont);
        lblCopyRight.setBounds((300-175)/2, 170, 175, 20);
        add(lblCopyRight);

        Font cbFont = new Font("Arial", Font.BOLD, 12);
        int cbHeight = 90;

        String[] inputHour = new String[12];
        for(int i = 0; i < 12; i++) {
            if(i < 10) {
                inputHour[i]  = "0" + i;
            } else {
                inputHour[i]  = "" + i;
            }
        }

        cbInputHour = new JComboBox(inputHour);
        cbInputHour.setSelectedIndex(0);
        hourSelected = cbInputHour.getSelectedIndex();
        cbInputHour.setFont(cbFont);
        cbInputHour.addActionListener(this::onHourComboSelection);
        cbInputHour.setBounds(120, cbHeight, 50, 30);
        add(cbInputHour);

        String[] inputMinute = new String[60];
        for(int i = 0; i < 60; i++) {
            if(i < 10) {
                inputMinute[i] = "0" + i;
            } else {
                inputMinute[i] = "" + i;
            }
        }
        cbInputMinute = new JComboBox(inputMinute);
        cbInputMinute.setSelectedIndex(0);
        minuteSelected = cbInputMinute.getSelectedIndex();
        cbInputMinute.setFont(cbFont);
        cbInputMinute.addActionListener(this::onMinuteComboSelection);
        cbInputMinute.setBounds(180, cbHeight, 50, 30);
        add(cbInputMinute);

        String[] inputSecond = new String[60];
        for(int i = 0; i < 60; i++) {
            if(i < 10)
                inputSecond[i] = "0" + i;
            else
                inputSecond[i] = "" + i;
        }
        cbInputSecond = new JComboBox(inputSecond);
        cbInputSecond.setSelectedIndex(0);
        secondSelected = cbInputSecond.getSelectedIndex();
        cbInputSecond.setFont(cbFont);
        cbInputSecond.addActionListener(this::onSecondComboSelection);
        cbInputSecond.setBounds(240, cbHeight, 50, 30);
        add(cbInputSecond);

        Font fontBtn = new Font("Courier", Font.PLAIN, 14);
        int btnWidth = 85;
        int btnHieght = 25;

        JButton btnOK = new JButton("OK");
        btnOK.setFont(fontBtn);
        btnOK.setBounds(105, 135, btnWidth, btnHieght);
        btnOK.addActionListener(this::onOkayClicked);
        add(btnOK);

        JButton btnCancel = new JButton("Cancel");
        btnCancel.setFont(fontBtn);
        btnCancel.setBounds(210, 135, btnWidth, btnHieght);
        btnCancel.addActionListener(this::onCancelClicked);
        add(btnCancel);
    }

    private void onSecondComboSelection(ActionEvent actionEvent) {
        if(actionEvent.getSource() == cbInputSecond) {
            secondSelected = cbInputSecond.getSelectedIndex();
        }
    }

    private void onMinuteComboSelection(ActionEvent actionEvent) {
        if(actionEvent.getSource() == cbInputMinute) {
            minuteSelected = cbInputMinute.getSelectedIndex();
        }
    }

    private void onHourComboSelection(ActionEvent actionEvent) {
        if(actionEvent.getSource() == cbInputHour) {
            hourSelected = cbInputHour.getSelectedIndex();
        }
    }

    private void onOkayClicked(ActionEvent e) {
        //Update Label
        MainPanel.lblCountDown.setText((hourSelected < 10 ? "0" + hourSelected : "" + hourSelected) +":" + (minuteSelected < 10 ? "0" + minuteSelected : "" + minuteSelected) + ":" + (secondSelected < 10 ? "0" + secondSelected : "" + secondSelected));
        //Update State
        MainPanel.btnCDStartPause.setEnabled(true);
        MainPanel.setEnableCountDown(true);
        MainPanel.setHourCountDown(hourSelected);
        MainPanel.setMinuteCountDown(minuteSelected);
        MainPanel.setSecondCountDown(secondSelected);
        MainPanel.setRestartHour(hourSelected);
        MainPanel.setRestartMinute(minuteSelected);
        MainPanel.setRestartSecond(secondSelected);
        MainPanel.cdInputFrame.setVisible(false);
    }

    private void onCancelClicked(ActionEvent actionEvent) {
        MainPanel.setEnableCountDown(false);
        MainPanel.btnCDStartPause.setEnabled(false);
        MainPanel.setHourCountDown(0);
        MainPanel.setAlarmMinute(0);
        MainPanel.setSecondCountDown(0);
        MainPanel.cdInputFrame.setVisible(false);
    }
}
