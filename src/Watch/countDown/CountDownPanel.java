package Watch.countDown;

import Watch.MainFrame;
import Watch.MainPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

class CountDownPanel extends JPanel {
    CountDownPanel() {
        setLayout(null);
        setVisible(true);
        setBounds(0, 0, 500, 500);

        JLabel countDownTitle = new JLabel("Count Down Clock");
        countDownTitle.setFont(new Font("Courier", Font.BOLD, 20));
        countDownTitle.setBounds((500 - 150)/2, 20, 150, 30);
        add(countDownTitle);
        //
        ImageIcon icon = new ImageIcon(MainFrame.class.getResource("/countDown_clock.gif"));
        JLabel alarmIcon = new JLabel(icon);
        alarmIcon.setBounds(45, 15, icon.getIconWidth(), icon.getIconHeight());
        add(alarmIcon);

        JLabel alarmRining = new JLabel("...Time's up...");
        alarmRining.setFont(new Font("Courier", Font.PLAIN, 18));
        alarmRining.setBounds((500 - 110)/2, 375, 150, 30);
        add(alarmRining);

        int WidthBtn = 85;
        int HieghtBtn = 25;

        JButton btnCDOk = new JButton("Ok");
        btnCDOk.setFont( new Font("Courier", Font.PLAIN, 14));
        btnCDOk.setBounds((500 - WidthBtn)/2, 420, WidthBtn, HieghtBtn);
        btnCDOk.addActionListener(this::onOkClick);
        add(btnCDOk);
    }

    private void onOkClick(ActionEvent e) {
        MainPanel.setEnableCountDown(false);
        MainPanel.setHourCountDown(0);
        MainPanel.setMinuteCountDown(0);
        MainPanel.setSecondCountDown(0);
        MainPanel.setCDStart(false);
        MainPanel.btnCDStartPause.setEnabled(false);
        MainPanel.btnCDStartPause.setText("Start");
        MainPanel.btnCDRestart.setEnabled(false);
        MainPanel.btnCDSetup.setEnabled(true);
        MainPanel.setSoundPlay(false);
        MainPanel.countDownAlertFrame.setVisible(false);
    }
}
