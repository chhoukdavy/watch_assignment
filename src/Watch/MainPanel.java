package Watch;

import Watch.alarm.AlarmFrame;
import Watch.alarm.InputDialogBoxFrame;
import Watch.countDown.CountDownFrame;
import Watch.countDown.CountDownInputDialogFrame;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Calendar;

public class MainPanel extends JPanel{
    private static JLabel lblDate;
    private static JLabel lblTime;
    private static Calendar cl = Calendar.getInstance();

    //Set Alarm Variable
    public static InputDialogBoxFrame inputFrameSetAlarm;
    public static AlarmFrame alarmFrame;
    public static JLabel lblAlarmIcon;
    private static int alarmHour;
    private static int alarmMinute;
    private static int alarmSecond;
    private static boolean isAlarmAm;

    private JButton btnAlarm;
    private JButton btnStopWatch;
    private JButton btnCountDown;
    private JButton btnExit;

    //StopWatch
    private static JLabel lblStopWatch;
    private JButton btnSWStartStop;
    private JButton btnSWPauseResume;
    private JButton btnSWBack;
    private static boolean enableSWKeyListenter = false;

    //CountDownWatch
    public static CountDownInputDialogFrame cdInputFrame;
    public static CountDownFrame countDownAlertFrame;
    public static JLabel lblCountDown;
    public static JButton btnCDStartPause;
    public static JButton btnCDRestart;
    public static JButton btnCDSetup;
    private JButton btnCDBack;

    MainPanel() {
        setLayout(null);
        setBounds(0, 0, 550, 350);
        setFocusable(true);

        //Main Panel GUI
        JLabel lblTitle = new JLabel("vWatch");
        Font fontTitle = new Font("Courier", Font.BOLD, 25);
        lblTitle.setFont(fontTitle);
        lblTitle.setBounds((550 - 90)/2, 10, 90, 50);
        add(lblTitle);

        JLabel lblCopyRight = new JLabel("@2016 all rights reserved.");
        Font fontRigthReserve = new Font("Courier", Font.PLAIN, 10);
        lblCopyRight.setFont(fontRigthReserve);
        lblCopyRight.setBounds((550 - 125)/2, 300, 125, 40);
        add(lblCopyRight);

        lblTime = new JLabel("00:00:00 AM");
        Font fontTime = new Font("Courier", Font.BOLD, 50);
        lblTime.setFont(fontTime);
        lblTime.setBounds((550 - 300)/2, 110, 300, 40);
        add(lblTime);

        lblDate = new JLabel("MMM, DD YYYY");
        Font fontDate = new Font("Courier", Font.BOLD, 35);
        lblDate.setFont(fontDate);
        lblDate.setBounds((550 - 225)/2, 165, 225, 30);
        add(lblDate);

        Font fontBtn = new Font("Courier", Font.PLAIN, 14);
        int btnWidth = 125;
        int btnHieght = 30;

        btnAlarm = new JButton("Set Alarm");
        btnAlarm.setFont(fontBtn);
        btnAlarm.setBounds(145, 225, btnWidth, btnHieght);
        btnAlarm.addActionListener(e -> onClickSetAlarm());
        add(btnAlarm);

        alarmFrame = new AlarmFrame(false);
        alarmFrame.setVisible(false);

        btnStopWatch = new JButton("Stop Watch");
        btnStopWatch.setFont(fontBtn);
        btnStopWatch.setBounds(290, 225, btnWidth, btnHieght);
        btnStopWatch.addActionListener(this::onStopWatchClick);
        add(btnStopWatch);

        btnCountDown = new JButton("Count Down");
        btnCountDown.setFont(fontBtn);
        btnCountDown.setBounds(145, 265, btnWidth, btnHieght);
        btnCountDown.addActionListener(this::onCountDownClick);
        add(btnCountDown);

        btnExit = new JButton("Exit");
        btnExit.setFont(fontBtn);
        btnExit.setBounds(290, 265, btnWidth, btnHieght);
        btnExit.addActionListener(e -> onClickExit());
        add(btnExit);
        //END Main Panel GUI

        //Stop Watch
        ImageIcon alarmIcon = new ImageIcon(MainFrame.class.getResource("/alarm_icon.gif"));
        lblAlarmIcon = new JLabel(alarmIcon);
        lblAlarmIcon.setBounds((550 - alarmIcon.getIconWidth())/2, 70, alarmIcon.getIconWidth(), alarmIcon.getIconHeight());
        lblAlarmIcon.setVisible(false);
        add(lblAlarmIcon);

        lblStopWatch = new JLabel("00:00:00");
        lblStopWatch.setVisible(false);
        lblStopWatch.setFont(fontTime);
        lblStopWatch.setBounds((550 - 215)/2, 110, 215, 40);
        add(lblStopWatch);

        btnSWStartStop = new JButton("Start");
        btnSWStartStop.setVisible(false);
        btnSWStartStop.setFont(fontBtn);
        btnSWStartStop.setBounds(140, 225, btnWidth, btnHieght);
        btnSWStartStop.addActionListener(this::onStartStop);
        add(btnSWStartStop);

        btnSWPauseResume = new JButton("Pause");
        btnSWPauseResume.setEnabled(false);
        btnSWPauseResume.setVisible(false);
        btnSWPauseResume.setFont(fontBtn);
        btnSWPauseResume.setBounds(285, 225, btnWidth, btnHieght);
        btnSWPauseResume.addActionListener(this::onPauseResume);
        add(btnSWPauseResume);

        btnSWBack = new JButton("Back");
        btnSWBack.setVisible(false);
        btnSWBack.setFont(fontBtn);
        btnSWBack.setBounds((550 - btnWidth)/2, 265, btnWidth, btnHieght);
        btnSWBack.addActionListener(this::onBackClick);
        add(btnSWBack);

        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                //if(enableSWKeyListenter)
                    onSWKeyPress(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        //END StopWatch

        //START Count Down Watch
        lblCountDown = new JLabel("00:00:00");
        lblCountDown.setVisible(false);
        lblCountDown.setFont(fontTime);
        lblCountDown.setBounds((550 - 215)/2, 110, 215, 40);
        add(lblCountDown);

        btnCDStartPause = new JButton("Start");
        btnCDStartPause.setEnabled(false);
        btnCDStartPause.setVisible(false);
        btnCDStartPause.setFont(fontBtn);
        btnCDStartPause.setBounds(140, 225, btnWidth, btnHieght);
        btnCDStartPause.addActionListener(this::onCDStartPause);
        add(btnCDStartPause);

        btnCDRestart = new JButton("Restart");
        btnCDRestart.setEnabled(false);
        btnCDRestart.setVisible(false);
        btnCDRestart.setFont(fontBtn);
        btnCDRestart.setBounds(285, 225, btnWidth, btnHieght);
        btnCDRestart.addActionListener(this::onCDRestart);
        add(btnCDRestart);

        btnCDSetup = new JButton("Setup");
        btnCDSetup.setVisible(false);
        btnCDSetup.setFont(fontBtn);
        btnCDSetup.setBounds(140, 265, btnWidth, btnHieght);
        btnCDSetup.addActionListener(this::onCDSetupClick);
        add(btnCDSetup);

        btnCDBack = new JButton("Back");
        btnCDBack.setVisible(false);
        btnCDBack.setFont(fontBtn);
        btnCDBack.setBounds(285, 265, btnWidth, btnHieght);
        btnCDBack.addActionListener(this::onCDBackClick);
        add(btnCDBack);
        //END Count Down Watch

        TimerX timer = new TimerX();
        timer.start();

        CalendarX cx = new CalendarX();
        cx.start();
    }

//    static int second = 45;
//    static int minute = 59;
//    static int hour = 11;
//    static boolean isAM = true;
    private static int second = cl.get(Calendar.SECOND);
    private static int minute = cl.get(Calendar.MINUTE);
    private static int hour = cl.get(Calendar.HOUR);
    private static boolean isAM = (cl.get(Calendar.AM_PM) == Calendar.AM);

    private class TimerX extends Thread {
        @Override
        public void run() {
            try {
                while (true) {
                        if(second  == 59) {
                            if(minute == 59) {
                                if(hour == 11) {
                                    isAM = !isAM;
                                }
                                hour = (hour + 1) % 13;
                            }
                            minute = (minute + 1) % 60;
                        }
                    second = (second + 1) % 60;
                    if(second == 0)
                        second++;
                    if(minute == 0)
                        minute++;
                    if(hour == 0)
                        hour++;
                    lblTime.setText((hour < 10 ? "0" + hour:hour)+ ":" + (minute < 10 ? "0" + minute: minute) + ":" + (second < 10 ? "0" + second: second) + " " + (isAM?"AM":"PM"));
                    Thread.sleep(1000);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class CalendarX extends Thread {
        int month = cl.get(Calendar.MONTH) + 1; //Zer0 based
        //int month = 12; //Zer0 based
        int day = cl.get(Calendar.DAY_OF_MONTH);
        //int day = lastDayOfTheMonth(month);
        int year = cl.get(Calendar.YEAR);

        @Override
        public void run() {
            try {
                while(true) {
                    if(isAM && hour == 11 && minute == 59 && second == 59) {
                        if(day == lastDayOfTheMonth(month)) {
                            if(month == 12) {
                                year++;
                            }
                            month = (month + 1) % 12;
                        }
                        day = (day + 1) % lastDayOfTheMonth(month);
                    }
                    lblDate.setText(toStringMonth(month) + ", " + day + " " + year);
                    Thread.sleep(1000);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private int lastDayOfTheMonth(int month) {
            int lastDay;
            switch (month) {
                case 1:
                    lastDay = 31;
                    break;
                case 2:
                    lastDay = 28;
                    if((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {
                        lastDay = 29;
                    }
                    break;
                case 3:
                    lastDay = 31;
                    break;
                case 4:
                    lastDay = 30;
                    break;
                case 5:
                    lastDay = 31;
                    break;
                case 6:
                    lastDay = 30;
                    break;
                case 7:
                    lastDay = 31;
                    break;
                case 8:
                    lastDay = 31;
                    break;
                case 9:
                    lastDay = 30;
                    break;
                case 10:
                    lastDay = 31;
                    break;
                case 11:
                    lastDay = 30;
                    break;
                case 12:
                    lastDay = 31;
                    break;
                default: lastDay = 0;

            }
            return lastDay;
        }

        private String toStringMonth(int month) {
            String monthR;
            switch (month){
                case 1:
                    monthR = "JAN";
                    break;
                case 2:
                    monthR = "FEB";
                    break;
                case 3:
                    monthR = "MAR";
                    break;
                case 4:
                    monthR = "APR";
                    break;
                case 5:
                    monthR = "MAY";
                    break;
                case 6:
                    monthR = "JUN";
                    break;
                case 7:
                    monthR = "JUL";
                    break;
                case 8:
                    monthR = "AUG";
                    break;
                case 9:
                    monthR = "SEP";
                    break;
                case 10:
                    monthR = "OCT";
                    break;
                case 11:
                    monthR = "NOV";
                    break;
                case 12:
                    monthR = "DEC";
                    break;
                default: monthR = "";
            }
            return monthR;
        }
    }

    //Real Time Getter/Setter
    public static int getMinute() {
        return minute;
    }
    public static int getHour() {
        return hour;
    }
    public static int getSecond() {
        return second;
    }
    public static boolean isAM() {
        return isAM;
    }
    //End Real Time Getter/Setter


    //Alarm Set Alarm
    private static boolean isAlarmClockThreadLive = true;
    private static boolean isAlarming = true;
    private static boolean enableAlarm = false;

    private void onClickSetAlarm() {
        isAlarmClockThreadLive = true;
        isAlarming = true;
        //Alarm Thread Start
        new Thread(() -> {
            try {
                while(isAlarmClockThreadLive) {
                    while(isAlarming) {
                        if(enableAlarm && hour == alarmHour && minute == alarmMinute && second == alarmSecond && isAM == isAlarmAm) {
                            alarmFrame.setVisible(true);
                            setSoundPlay(true);
                            PlaySound playSoundAlarm = new PlaySound();
                            playSoundAlarm.start();
                            enableAlarm = false;
                        }
//                        System.out.println("Enable Alarm: " + enableAlarm);
//                        System.out.println("Hour: " + hour + "  /t/t AlarmHour: " + alarmHour);
//                        System.out.println("Hour Compare: " + (hour == alarmHour));
//                        System.out.println("Min: " + minute + " /t/t AlarmMin: " + alarmMinute);
//                        System.out.println("Minute Compare: " + (minute == alarmMinute));
//                        System.out.println("Sec: " + second + " /t/t AlarmSec: " + alarmSecond);
//                        System.out.println("Second Compare: " + (second == alarmSecond));
//                        System.out.println("AMPM: " + isAM + " /t/t AlarmAMPM: " + isAlarmAm);
//                        System.out.println("AMPM Compare: " + (isAM == isAlarmAm));
//                        System.out.println("--------------------------------------------------------/n/n");
                        Thread.sleep(1000);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        inputFrameSetAlarm = new InputDialogBoxFrame(true);
    }
    //Alarm Getter/Setter
    public static void setAlarmHour(int alarmHour) {
        MainPanel.alarmHour = alarmHour;
    }
    public static void setAlarmMinute(int alarmMinute) {
        MainPanel.alarmMinute = alarmMinute;
    }
    public static void setAlarmSecond(int alarmSecondInput) {
        MainPanel.alarmSecond = alarmSecondInput;
    }
    public static void setAlarmSnooze(int snoozeSecond) {
        MainPanel.alarmSecond = (second + snoozeSecond);
        if(MainPanel.alarmSecond >59) {
            MainPanel.alarmMinute++;
        }
        MainPanel.alarmSecond %= 60;
    }
    public static void setIsAlarmAm(boolean isAlarmAm) {
        MainPanel.isAlarmAm = isAlarmAm;
    }
    public static void setIsAlarmClockThreadLive(boolean value) {
        isAlarmClockThreadLive = value;
    }
    public static void setIsAlarming(boolean isAlarming) {
        MainPanel.isAlarming = isAlarming;
    }
    public static void setEnableAlarm(boolean enableAlarm) {
        MainPanel.enableAlarm = enableAlarm;
    }

    //END Alarm Getter/Setter
    //END Alarm Set Alarm

    //StopWatch Event Handler
    private void onStopWatchClick(ActionEvent e) {
        //Clear MainPanel
        lblDate.setVisible(false);
        lblTime.setVisible(false);
        btnAlarm.setVisible(false);
        btnStopWatch.setVisible(false);
        btnCountDown.setVisible(false);
        btnExit.setVisible(false);

        //Show MainPanel StopWatch
        lblStopWatch.setVisible(true);
        btnSWStartStop.setVisible(true);
        btnSWPauseResume.setVisible(true);
        btnSWBack.setVisible(true);

        enableSWKeyListenter = true;
        isSWthreadDie = false;
        //Start Thread
        new Thread(() -> {
            while (true) {
                //System.out.println("SW Thread has started...");
                while(isStart) {
                    try {
                        if(!isPause) {
                            if(swSecond == 59) {
                                if(swMinute == 59) {
                                    swHour++;
                                }
                                swMinute = (swMinute + 1) % 60;
                            }
                            swSecond = (swSecond + 1) % 60;
                        }
                        lblStopWatch.setText((swHour < 10 ? "0" + swHour : "" + swHour) + ":" + (swMinute < 10 ? "0" + swMinute : ""+ swMinute) + ":" + (swSecond < 10 ? "0" + swSecond : "" + swSecond));
                        Thread.sleep(1000);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                if(isSWthreadDie) {
                    break;
                }
            }
        }).start();
    }

    private int swHour;
    private int swMinute;
    private int swSecond;
    private boolean isStart = false;
    private boolean isPause = false;
    private boolean isSWthreadDie = false;

    private void onStartStop(ActionEvent e) {
        isStart = !isStart;
        btnSWStartStop.setText("Stop");
        swHour = 0;
        swMinute = 0;
        swSecond = 0;
        if(!isStart) {
            btnSWStartStop.setText("Start");
            btnSWPauseResume.setEnabled(false);
            isPause = false;
            btnSWPauseResume.setText("Pause");
        } else {
            btnSWPauseResume.setEnabled(true);
        }
    }
    private void onPauseResume(ActionEvent e) {
        isPause = !isPause;
        if(isPause) {
            btnSWPauseResume.setText("Resume");
        } else {
            btnSWPauseResume.setText("Pause");
        }
    }
    private void onBackClick(ActionEvent e) {
        swHour = 0;
        swMinute = 0;
        swSecond = 0;
        isStart = false;
        btnSWStartStop.setText("Start");
        btnSWPauseResume.setEnabled(false);
        lblStopWatch.setText("00:00:00");
        isPause = false;
        btnSWPauseResume.setText("Pause");

        //Clear MainPanel
        lblStopWatch.setVisible(false);
        btnSWStartStop.setVisible(false);
        btnSWPauseResume.setVisible(false);
        btnSWBack.setVisible(false);

        //Show MainPanel StopWatch
        lblDate.setVisible(true);
        lblTime.setVisible(true);
        btnAlarm.setVisible(true);
        btnStopWatch.setVisible(true);
        btnCountDown.setVisible(true);
        btnExit.setVisible(true);

        enableSWKeyListenter = false;
        isSWthreadDie = true;
    }
    private void onSWKeyPress(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if(enableSWKeyListenter) {
            if(keyCode == KeyEvent.VK_ENTER) {
//                System.out.println("Key Enter Press is working...!");
                swHour = 0;
                swMinute = 0;
                swSecond = 0;
                isStart = !isStart;
                btnSWStartStop.setText("Stop");

                if(isStart) {
                    btnSWPauseResume.setEnabled(true);
                } else {
                    btnSWStartStop.setText("Start");
                    btnSWPauseResume.setEnabled(false);
                    isPause = false;
                    btnSWPauseResume.setText("Pause");
                }
            }
            if(isStart) {
                if(keyCode == KeyEvent.VK_SPACE){
//                    System.out.println("Space Key is Working...");
                    isPause = !isPause;
                    if(isPause) {
                        btnSWPauseResume.setText("Resume");
                    } else {
                        btnSWPauseResume.setText("Pause");
                    }
                }
            }
        }
    }
    //End Watch Event Handler

    //START Count Down Event Handler
    private static boolean enableCountDown;
    private static int hourCountDown;
    private static int minuteCountDown;
    private static int secondCountDown;
    static boolean CDThreadAlive = false;
    static boolean CDStart = false;
    static boolean enableRestart = false;
    static boolean soundPlay = false;

    private void onCountDownClick(ActionEvent actionEvent) {
        //Clear MainPanel
        lblDate.setVisible(false);
        lblTime.setVisible(false);
        btnAlarm.setVisible(false);
        btnStopWatch.setVisible(false);
        btnCountDown.setVisible(false);
        btnExit.setVisible(false);

        //Show Count Down GUI
        lblCountDown.setVisible(true);
        btnCDStartPause.setVisible(true);
        btnCDRestart.setVisible(true);
        btnCDSetup.setVisible(true);
        btnCDBack.setVisible(true);
    }

    private void onCDStartPause(ActionEvent actionEvent) {
        btnCDSetup.setEnabled(false);
        enableRestart = !enableRestart;
        CDStart = !CDStart;
        if(CDStart)
            btnCDStartPause.setText("Pause");
        else
            btnCDStartPause.setText("Start");
        btnCDRestart.setEnabled(!CDStart);

        CDThreadAlive = true;
        enableCountDown = true;

        new Thread(() -> {
            while (true) {
                while(enableCountDown) {
                    try {
                        if(CDStart) {
                            if (secondCountDown == 0) {
                                if (minuteCountDown == 0) {
                                    hourCountDown--;
                                    minuteCountDown = 60;
                                }
                                if (minuteCountDown > 0) {
                                    minuteCountDown--;
                                    secondCountDown = 60;
                                }
                            }
                            secondCountDown--;
                        }
                        //setLabelCountDown
                        lblCountDown.setText((hourCountDown >= 10 ? "" + hourCountDown : "0" + hourCountDown) + ":" + (minuteCountDown >= 10 ? "" + minuteCountDown : "0" + minuteCountDown) + ":" + (secondCountDown >= 10 ? "" + secondCountDown : "0" + secondCountDown));
                        //check Alert
                        if (hourCountDown == 0 && minuteCountDown == 0 && secondCountDown == 0) {
                            countDownAlertFrame = new CountDownFrame(true);
                            soundPlay = true;
                            PlaySound playSound = new PlaySound();
                            playSound.start();
                            enableCountDown = false;
                        }
                        Thread.sleep(1000);
                    } catch (Exception e11) {
                        e11.printStackTrace();
                    }
                }
                if(CDThreadAlive) {
                    break;
                }
            }
        }).start();
    }

    static int restartHour;
    static int restartMinute;
    static int restartSecond;

    private void onCDRestart(ActionEvent actionEvent) {
        MainPanel.CDStart = false;
        MainPanel.btnCDStartPause.setText("Start");
        MainPanel.hourCountDown = restartHour;
        MainPanel.minuteCountDown = restartMinute;
        MainPanel.secondCountDown = restartSecond;
        lblCountDown.setText((hourCountDown >= 10 ? "" + hourCountDown : "0" + hourCountDown) + ":" + (minuteCountDown >= 10 ? "" + minuteCountDown : "0" + minuteCountDown) + ":" + (secondCountDown >= 10 ? "" + secondCountDown : "0" + secondCountDown));
    }

    private void onCDSetupClick(ActionEvent actionEvent) {
        cdInputFrame = new CountDownInputDialogFrame(true);
    }
    private void onCDBackClick(ActionEvent actionEvent) {
        //Clear Count Down GUI
        lblCountDown.setVisible(false);
        btnCDStartPause.setVisible(false);
        btnCDRestart.setVisible(false);
        btnCDSetup.setVisible(false);
        btnCDBack.setVisible(false);

        //Clear State
        MainPanel.CDThreadAlive = false;
        MainPanel.enableCountDown = false;
        MainPanel.setHourCountDown(0);
        MainPanel.setMinuteCountDown(0);
        MainPanel.setSecondCountDown(0);
        lblCountDown.setText((hourCountDown >= 10 ? "" + hourCountDown : "0" + hourCountDown) + ":" + (minuteCountDown >= 10 ? "" + minuteCountDown : "0" + minuteCountDown) + ":" + (secondCountDown >= 10 ? "" + secondCountDown : "0" + secondCountDown));
        MainPanel.CDStart = false;
        MainPanel.btnCDStartPause.setEnabled(false);
        MainPanel.btnCDStartPause.setText("Start");
        MainPanel.btnCDRestart.setEnabled(false);
        MainPanel.btnCDSetup.setEnabled(true);

        //Show MainPanel
        btnAlarm.setVisible(true);
        btnStopWatch.setVisible(true);
        btnCountDown.setVisible(true);
        btnExit.setVisible(true);
        lblDate.setVisible(true);
        lblTime.setVisible(true);
    }

    //Getter/Setter CountDown

    public static void setEnableCountDown(boolean enableCountDown) {
        MainPanel.enableCountDown = enableCountDown;
    }

    public static void setHourCountDown(int hourCountDown) {
        MainPanel.hourCountDown = hourCountDown;
    }

    public static void setMinuteCountDown(int minuteCountDown) {
        MainPanel.minuteCountDown = minuteCountDown;
    }

    public static void setSecondCountDown(int secondCountDown) {
        MainPanel.secondCountDown = secondCountDown;
    }

    public static void setCDThreadAlive(boolean CDThreadAlive) {
        MainPanel.CDThreadAlive = CDThreadAlive;
    }

    public static void setCDStart(boolean CDStart) {
        MainPanel.CDStart = CDStart;
    }

    public static void setEnableRestart(boolean enableRestart) {
        MainPanel.enableRestart = enableRestart;
    }

    public static void setRestartHour(int restartHour) {
        MainPanel.restartHour = restartHour;
    }

    public static void setRestartMinute(int restartMinute) {
        MainPanel.restartMinute = restartMinute;
    }

    public static void setRestartSecond(int restartSecond) {
        MainPanel.restartSecond = restartSecond;
    }

    public static void setSoundPlay(boolean soundPlay) {
        MainPanel.soundPlay = soundPlay;
    }

    //End Getter/Setter CountDown
    //END Count Down Event Handler

    //START Exit Button Event
    private void onClickExit() {
        System.exit(0);
    }
    //End Exit Button Event

    class PlaySound extends Thread{
        @Override
        public void run() {
            while(soundPlay) {
                try {
                    AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(MainFrame.class.getResource("/ring_sound.wav"));
                    Clip clip = AudioSystem.getClip();
                    clip.open(audioInputStream);
                    clip.start();
                    Thread.sleep(5000);
                } catch (Exception e22) {
                    e22.printStackTrace();
                }
            }
        }
    }
}
