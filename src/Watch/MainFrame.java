package Watch;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                MainFrame frame = new MainFrame();
                frame.setVisible(true);
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private MainFrame() {
        setUndecorated(true);
        //setResizable(false);
        Point centerP = centerPoint.getCenterPoint(550, 350);
        setBounds(centerP.x, centerP.y, 550, 350);
        setContentPane(new MainPanel());
    }
}
